<?php

// Dequeue the slider scripts.
function soshal_enqueue_scripts() {
  wp_enqueue_script("prevent_slider", get_stylesheet_directory_uri() . "/js/prevent_slider.js");
}

add_action("wp_enqueue_scripts", "soshal_enqueue_scripts");

?>
