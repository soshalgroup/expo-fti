/*!
 * prevent_slider.js
 *
 * Prevents the homepage slider from functioning, as it isn't editable by the
 * client in the theme.
 */

;(function ($, window, document, undefined) {

    "use strict";

    $(function() {

        $('.oneByOne_item').each(function(index) {

            if (index > 0) {
                $(this).remove();
            }

        });

        $('.oneByOne_item').on('mousedown', function(event) {
            event.preventDefault();
            event.stopPropagation();
        });

    });

})(jQuery, window, document);
