<?php ob_start(); ?>
<div class="item">
<?php $o = new wpdreamsYesNo("asp_caching", "Caching activated", postval_or_getoption('asp_caching')); ?>
</div>
<div class="item">
<?php $o = new wpdreamsText("asp_cachinginterval", "Caching interval (in minutes, default 1440, aka. 1 day)", postval_or_getoption('asp_cachinginterval'), array( array("func"=>"ctype_digit", "op"=>"eq", "val"=>true)) ); ?>
</div>
<div class="item">
<?php $o = new wpdreamsYesNo("asp_precacheimages", "Precache images", postval_or_getoption('asp_precacheimages')); ?>
</div>
<div class="item">
  <input type='submit' class='submit' value='Save options'/>
</div>
<?php $_r = ob_get_clean(); ?>


<?php
  $updated = false;
  if (isset($_POST) && (wpdreamsType::getErrorNum()==0)) {
    foreach($_POST as $key=>$value) {
      if (is_string($key) && (strpos('asp_', $key)==0)) {
        update_option($key, $value);
        $updated = true;
      }
    }
  }
?>
<?php
$_comp = wpdreamsCompatibility::Instance();
if ($_comp->has_errors()): 
?>
<div class="wpdreams-slider errorbox">
        <p class='errors'>Possible incompatibility! Please go to the <a href="<?php echo get_admin_url()."admin.php?page=ajax-search-pro/comp_check.php"; ?>">error check</a> page to see the details and solutions!</p> 
</div>
<?php endif; ?>
<div class='wpdreams-slider'>
<form name='caching' method='post'>
  <?php if($updated): ?><div class='successMsg'>Search Form Successfuly added!</div><?php endif; ?>
  <fieldset>
    <legend>Caching Options</legend>
    <?php print $_r; ?> 
  </fieldset>
</form>
  <fieldset>
    <legend>Clear Cache</legend>
    <div class="item">
      <input type='submit' class="red" name='Clear Cache' id='clearcache' value='Clear the cache!'>
    </div>                                                            
  </fieldset>
</div> 
<script>
   jQuery(document).ready((function($) {
      $('#clearcache').on('click', function(){
        var r = confirm('Do you really want to clear the cache?');
        if (r!=true) return;
        var button = $(this);
        var data = {
          action: 'ajaxsearchpro_deletecache'  
        };
        button.attr("disabled", true);
        var oldVal = button.attr("value");
        button.attr("value", "Loading...");
        button.addClass('blink');
        $.post(ajaxsearchpro.ajaxurl, data, function(response) {
           var currentdate = new Date();
           var datetime =  currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " @ "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();
           button.attr("disabled", false);
           button.removeClass('blink');
           button.attr("value", oldVal);
           button.parent().parent().append('<div class="successMsg">Cache succesfully cleared! '+response+' file(s) deleted at '+datetime+'</div>');
        }, "json"); 
      });
   })(jQuery));
</script>        