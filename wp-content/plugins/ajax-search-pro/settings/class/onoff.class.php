<?php
if (!class_exists("wpdreamsOnOff")) {
  class wpdreamsOnOff extends wpdreamsType {
  	function getType() {
  		parent::getType();
      echo "<div class='wpdreamsOnOff'>";
  		echo "<label for='wpdreamstext_" . self::$_instancenumber . "'>" . $this->label . "</label>";
  		echo "<a class='wpdreamsonoff" . (($this->data == 1) ? " on" : " off") . "' id='wpdreamsonoff_" . self::$_instancenumber . "' name='" . $this->name . "_onoff'>" . (($this->data == 1) ? "ON" : "OFF") . "</a>";
  		echo "<input type='hidden' value='" . $this->data . "' name='" . $this->name . "'>";
  	  echo "<div class='triggerer'></div>
      </div>";
    }
  }
}
?>